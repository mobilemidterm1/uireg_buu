import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'profile.dart';
import 'reg_page.dart';

void main() {
  runApp(const RegApp());
}

class RegApp extends StatelessWidget {
  const RegApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('มหาวิทยาลัยบูรพา'),
          ),
          drawer: Drawer(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text('Kittipos Teppana'),
                  accountEmail: Text('62160240@go.buu.ac.th'),
                  currentAccountPicture: CircleAvatar(
                    child: Text("K"),
                    foregroundColor: Colors.white,
                    backgroundColor: Color.fromARGB(255, 140, 154, 166),
                  ),
                  otherAccountsPictures: <Widget>[],
                ),
                ListTile(
                  title: Text('ประวัตินิสิต'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => StudentProfile()),
                    );
                  },
                ),
                Divider(),
                ListTile(
                  title: Text('ตรวจสอบวันจบการศึกษา'),
                  onTap: () {},
                ),
                Divider(),
                ListTile(
                  title: Text('ตารางเรียน/สอบ'),
                  onTap: () {},
                ),
                Divider(),
                ListTile(
                  title: Text('ปฎิทินการศึกษา'),
                  onTap: () {},
                ),
                Divider(),
                ListTile(
                  title: Text('ติดต่อเรา'),
                  onTap: () {},
                ),
                Divider(),
                ListTile(
                  title: Text('วิชาที่เปิดสอน'),
                  onTap: () {},
                ),
                Divider(),
                ListTile(
                  title: Text('ระเบียบข้อบังคับ'),
                  onTap: () {},
                ),
                Divider(),
                ListTile(
                  title: Text('ออกจากระบบ'),
                  onTap: () {},
                ),
                Divider(),
              ],
            ),
          ),
          body: const ContactProfilePage(),
        ),
      ),
    );
  }
}
