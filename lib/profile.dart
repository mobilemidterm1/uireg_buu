import 'package:flutter/material.dart';

class StudentProfile extends StatelessWidget {
  const StudentProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyProfileWidget(),
      //   child: ElevatedButton(
      //     onPressed: () {
      //       Navigator.pop(context);
      //     },
      //     child: const Text('Go back!'),
      //   ),

    );
  }
}
AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.yellow[400],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
          color: Colors.white,
        )
    ),
    title: Text("บัญชีผู้ใช้งาน"),
    actions: <Widget>[
      // IconButton(
      //     onPressed: (){},
      //     icon: Icon(
      //       Icons.print,
      //       color: Colors.white,
      //     )
      // ),
    ],
  );
}

Widget buildBodyProfileWidget(){
  return Container(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          buildCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildAccountWidget(),
          const SizedBox(
            height: 10,
          ),
          buildStdCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildPersonalCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildOthPerCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildMoreDataCardWidget(),
          const SizedBox(
            height: 10,
          ),
        ],
      )
  ) ;
}

Widget buildCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.yellowAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 150,
                  height: 120,
                  child: Image.network(
                    "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/rick-and-morty-season-six-1662104017.jpg?crop=0.289xw:0.511xh;0.241xw,0.432xh&resize=480:*",
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Text('มหาวิทยาลัยบูรพา\n'
                    'Burapha University \n'
                    'คณะวิทยาการสารสนเทศ \n'
                    'Faculty of Informatics \n'
                    )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildAccountWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.yellowAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'บัญชีผู้ใช้งาน',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              color: Colors.pink[50],
              child: Column(
                children: [
                  Text(
                    '\n"13 Days"\n',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    '  จำนวนวันที่ใช้รหัสผ่านได้  \n',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ), //S
            Text(
              'รหัสผ่านหมดอายุ: 17 พฤษภาคม 2566 [23:59:00]\n',

              style: TextStyle(color: Colors.red[900]),
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 144,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.yellow[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('เปลี่ยนรหัสผ่าน'),
                      //Icon(Icons.touch_app),
                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildStdCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.yellowAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'ข้อมูลด้านการศึกษา',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
              // subtitle: Text(
              //   'Secondary Text',
              //   style: TextStyle(color: Colors.black.withOpacity(0.6)),
              // ),
            ),
            Text(
              '\nรหัสประจำตัว: 62160240\n'
                  'เลขที่บัตรประชาชน: 1354688526994\n'
                  'ชื่อ: นายกิตติพศ เทพนา \n'
                  'ชื่ออังกฤษ: MR. KITTIPOS TEPPANA \n'
                  'คณะ: คณะวิทยาการสารสนเทศ\n'
                  'วิทยาเขต: บางแสน\n'
                  'หลักสูตร: 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n'
                  'วิชาโท: -\n'
                  'ระดับการศึกษา: ปริญญาตรี\n'
                  'ชื่อปริญญา: วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n'
                  'ปีการศึกษาที่เข้า: 2562/1 (17/6/25632)\n'
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.yellow[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),

                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildPersonalCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.yellowAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'ข้อมูลส่วนบุคคล',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '\nสัญชาติ: ไทย ศาสนา: พุทธ\n'
                  'หมู่เลือด: ไม่ระบุ\n'
                  'ชื่อมารดา: สมหมาย เทพนา\n'
                  'ที่อยู่: 146 หมู่5 นาดง เขต/อำเภอ ปากคาด บึงกาฬ 38190\n'
                  'โทร: 0914546372\n'
                  'ผู้ปกครอง: วิรัตน์ เทพนา\n'
                  'ที่อยู่ (ผู้ปกครอง): 146 หมู่5 นาดง เขต/อำเภอ ปากคาด บึงกาฬ 38190\n'
                  'โทร: 0647243419\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.yellow[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),

                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildOthPerCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.yellowAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'บุคคลที่สามารถติดต่อได้',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '\nชื่อ: นาย วิรัตน์ เทพนา \n'
                  'ที่อยู่: 146 หมู่5 นาดง เขต/อำเภอ ปากคาด บึงกาฬ 38190\n'
                  'โทร: 0647243419\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.yellow[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),

                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildMoreDataCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.yellowAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'ข้อมูลเพิ่มเติมอื่นๆ',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '\nความพิการ: ไม่พิการ\n'
                  'สถานภาพการรับทุน: ไม่ได้รับทุน\n'
                  'จำนวนพี่น้องทั้งหมด (รวมตัวเอง): 3\n'
                  'นิสิตเป็นบุตรคนที่: 3\n'
                  'จำนวนพี่น้องที่กำลังศึกษาอยู่(รวมตัวเอง): 1\n'
                  'สถานภาพของมารดา: มีชีวิต\n'
                  'รายได้บิดา: < 200,000 บาทต่อปี (> 15,000 บาทต่อเดือน)\n'
                  'อาชีพบิดา: เกษตร\n'
                  'สถานภาพของมารดา: มีชีวิต\n'
                  'สภาพการสมรสของบิดามารดา: อยู่ด้วยกัน\n'
                  'รายได้ผู้ปกครอง: < 200,000 บาทต่อปี (< 15,000 บาทต่อเดือน)\n'
                  'อาชีพผู้ปกครอง: รับราชการ\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.yellow[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),

                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

