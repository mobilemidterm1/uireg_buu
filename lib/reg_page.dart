import 'package:flutter/material.dart';
import 'profile.dart';
import 'TableStudent.dart';

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatelessWidget {
  const ContactProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  //Height constraint at Container widget level
                  height: 180,
                  child: Image.network(
                    "https://image-tc.galaxy.tf/wijpeg-60yh5qi5rm1tveq7xu0klfi51/burapha-university_standard.jpg?crop=0%2C0%2C555%2C416&width=1140",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("ประกาศเรื่อง",
                          style: TextStyle(color: Color.fromARGB(255, 255, 162, 0), fontSize:30),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 40,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("กำหนดวันรับพระราชทานปริญญาบัตร"
                            "ปีการศึกษา 2566",
                          style: TextStyle(fontSize:15),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 450,
                  //Height constraint at Container widget level
                  height: 250,
                  child: Image.network("https://scontent.fbkk23-1.fna.fbcdn.net/v/t39.30808-6/325127416_911271559880903_8966534387829362571_n.jpg?stp=dst-jpg_p843x403&_nc_cat=100&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeEoyoo4ANelS_7KHCpadFf5MZT_Rtww0OsxlP9G3DDQ65IxyJ8kL3iIG1bz0lA4J1GsTogDGtGtpCgWh3BNhV9F&_nc_ohc=ZTpd_ApHM5UAX_PVZCg&_nc_ht=scontent.fbkk23-1.fna&oh=00_AfCWZIA5RMstmN3uibGx--co7EyxRhj5OJ0E4IIGm-MhYw&oe=63DFB963",
                    errorBuilder: (context,url, error) => Icon(Icons.error)
                  )
                ),
                Divider(
                  color: Colors.grey,
                ),

                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildProfileButton(context),
                      buildCalendarButton(),
                      buildTableButton(context),

                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildMessageButton(),
                      buildTableViewButton(),
                      buildRecentButton(),

                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildProfileButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.account_box_rounded ,
          color: Colors.grey.shade500,
        ),
        onPressed: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => StudentProfile()),
           );
         },

      ),
      Text("บัญชีผู้ใช้งาน"),
    ],
  );
}

Widget buildCalendarButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.edit_calendar_rounded ,
          color: Colors.grey.shade500,
        ),
        onPressed: () {},
      ),
      Text("ตรวจสอบวันจบ"
          "การศึกษา"),
    ],
  );
}

Widget buildTableButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.list_alt_outlined ,
          color: Colors.grey.shade500,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => StudentTable()),
          );
        },
      ),
      Text("ตารางเรียน/สอบ"),
    ],
  );
}

Widget buildMessageButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message ,
          color: Colors.grey.shade500,
        ),
        onPressed: () {},
      ),
      Text("ภาระค่าใช้จ่ายทุน"),
    ],
  );
}

Widget buildRecentButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.recent_actors ,
          color: Colors.grey.shade500,
        ),
        onPressed: () {},
      ),
      Text("หลักสูตรที่เปิดสอน"),
    ],
  );
}

Widget buildTableViewButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.table_view  ,
          color: Colors.grey.shade500,
        ),
        onPressed: () {},
      ),
      Text("ปฎิทินการศึกษา"),
    ],
  );
}


// enum APP_THEME { LIGHT, DARK }

// void main() {
//   runApp(ContactProfilePage());
// }

// class MyAppTheme {
//   static ThemeData appThemeLight() {
//     return ThemeData(
//         brightness: Brightness.light,
//         appBarTheme: AppBarTheme(
//           color: Colors.white,
//           iconTheme: IconThemeData(
//             color: Colors.black,
//           ),
//         ),
//         iconTheme: IconThemeData(
//           color: Color.fromARGB(255, 59, 79, 235),
//         ));
//   }

//   static ThemeData appThemeDark() {
//     return ThemeData(
//       brightness: Brightness.dark,
//     );
//   }
// }

// class ContactProfilePage extends StatefulWidget {
//   @override
//   State<ContactProfilePage> createState() => _ContactProfilePageState();
// }

// class _ContactProfilePageState extends State<ContactProfilePage> {
//   var currentTheme = APP_THEME.LIGHT;
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       theme: currentTheme == APP_THEME.DARK
//           ? MyAppTheme.appThemeLight()
//           : MyAppTheme.appThemeDark(),
//       home: Scaffold(
//         //appBar: buildAppBerWidget(),
//         // body: buildBodyWidget(),
//         floatingActionButton: FloatingActionButton(
//           child: Icon(Icons.brightness_4),
//           onPressed: () {
//             setState(() {
//               currentTheme == APP_THEME.DARK
//                   ? currentTheme = APP_THEME.LIGHT
//                   : currentTheme = APP_THEME.DARK;
//             });
//           },
//         ),
//       ),
//     );
//   }
// }
  
