import 'package:flutter/material.dart';

class StudentTable extends StatelessWidget {
  const StudentTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyStdTable(),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.yellow[400],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined,
          color: Colors.white,
        )),
    title: Text("ตารางเรียน/สอบ"),
    actions: <Widget>[],
  );
}

Widget buildBodyStdTable() {
  return Container(
    padding: EdgeInsets.all(6),
    child: ListView(
      children: [
        // buildDtaStdWidget(),
        // buildSelectYearWidget(),
        // buildSelectDurationWidget(),
        // Divider(
        //   color: Colors.pink[400],
        // ),
        buildScheduleWidget(),
        buildFinalWidget(),
      ],
    ),
  );
}

Widget buildScheduleWidget() {
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "ตารางเรียนรายวิชาที่ลงทะเบียนไว้แล้ว",
            textScaleFactor: 1.5,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.amber.shade100,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.pink.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
                  title: const Text(
                    'วันอังคาร',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 300,
                  child: Center(
                    child: Text('10:00-11:50\n'
                        '88634259-59 Multimedia Programming for Multiplatforms การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม\n'
                        '(3) 2, IF-4C02 [IF]\n'
                        '\n88622164 Algorithm Design and Application การออกแบบขั้นตอนวิธีและการประยุกต์\n'
                        '(3) 2, IF-6T04 [IF]\n'),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.green.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
                  title: const Text(
                    'วันพุธ',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 300,
                  child: Center(
                      child: Text('10:00-11:50\n'
                          '88634459-59 Mobile Application Development I การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1\n'
                          '(3) 2, IF-4C02 [IF]\n'
                          '\n13:00-15:00\n'
                          '88634259-59 Multimedia Programming for Multiplatforms การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม\n'
                          '(3) 2, IF-4C01 [IF]\n')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.blue.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
                  title: const Text(
                    'วันศุกร์',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  width: 300,
                  height: 200,
                  child: Center(
                    child: Text('09:00-11:50\n'
                        '88634459-59 	Introduction to Natural Language Processing การประมวลผลภาษาธรรมชาติเบื้องต้น\n'
                        '(3) 2, IF-5T05 [IF]\n'
                        '\n13:00-15:00\n'
                        '88634459-59 Mobile Application Development I การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1\n'
                        '(3) 2, IF-4C02 [IF]\n'),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
              '* ข้อมูลที่ปรากฎอยู่ในตารางเรียนประกอบด้วย รหัสวิชา (จำนวนหน่วยกิต) กลุ่ม, ห้องเรียนและอาคาร ตามลำดับ',
              textScaleFactor: 0.9),
        ),
      ]);
}

Widget buildFinalWidget() {
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
              'หมายเหตุ   C = Lecture  L = Lab  R = ประชุม  S = Self Study  T = ติว',
              textScaleFactor: 0.9),
        ),
      ]);
}
